package com.workshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workshop.dao.IEspecialistaDAO;
import com.workshop.models.Especialista;
import com.workshop.service.IEspecialistaService;

@Service
public class EspecialistaService implements IEspecialistaService {

	@Autowired
	IEspecialistaDAO service;
	@Override
	public Especialista persist(Especialista e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Especialista> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Especialista findBYId(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Especialista merge(Especialista e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}