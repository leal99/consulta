package com.workshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workshop.dao.IPacienteDAO;
import com.workshop.models.Paciente;
import com.workshop.service.IPacienteService;

@Service
public class PacienteService implements IPacienteService {

	@Autowired
	IPacienteDAO service;
	@Override
	public Paciente persist(Paciente e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Paciente> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Paciente findBYId(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Paciente merge(Paciente e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

	

}