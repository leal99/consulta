package com.workshop.service;

import java.util.List;

import com.workshop.models.Especialista;

public interface IEspecialistaService {
	Especialista persist(Especialista e);
	List<Especialista> getAll();
	Especialista findBYId(Integer id);
	Especialista merge(Especialista e);
	void delete(Integer id);

}
