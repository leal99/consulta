package com.workshop.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;

@Entity
public class Especialista {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id",nullable=false,unique=true)
	
	private Integer id;
	@Column(name="nombreEspecialista",length=200,nullable=false,unique=true)
	private String nombreEspecialista;
	@Column(name="rutEspecialista",length=200,nullable=false,unique=true)
	private String rutEspecialista;
	@Column(name="emailEspecialista",length=100,nullable=false,unique=true)
	private String emailEspecialista;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombreEspecialista() {
		return nombreEspecialista;
	}
	public void setNombreEspecialista(String nombreEspecialista) {
		this.nombreEspecialista = nombreEspecialista;
	}
	public String getRutEspecialista() {
		return rutEspecialista;
	}
	public void setRutEspecialista(String rutEspecialista) {
		this.rutEspecialista = rutEspecialista;
	}
	public String getEmailEspecialista() {
		return emailEspecialista;
	}
	public void setEmailEspecialista(String emailEspecialista) {
		this.emailEspecialista = emailEspecialista;
	}
	
	public List<EspecialidadMedica> getEspecialidadmedicas() {
		return especialidadmedicas;
	}
	public void setEspecialidadmedicas(List<EspecialidadMedica> especialidadmedicas) {
		this.especialidadmedicas = especialidadmedicas;
	}

	/*clave foranea sin Primary Key*/
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="Especialista_EspecialidadMedica", joinColumns=@JoinColumn(name="id_Especialista",referencedColumnName="id"),inverseJoinColumns=@JoinColumn(name="id_EspecialidadMedica",referencedColumnName="id"))   
    private List<EspecialidadMedica> especialidadmedicas;
}
