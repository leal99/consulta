/*
package com.workshop.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import com.workshop.models.EspecialidadMedica;
import com.workshop.models.Especialista;
@Entity
public class MedicoEspecialista {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Especialista getEspecialista() {
		return Especialista;
	}
	public void setEspecialista(Especialista especialista) {
		this.Especialista = especialista;
	}
	public EspecialidadMedica getEspecialidadMedica() {
		return EspecialidadMedica;
	}
	public void setEspecialidadMedica(EspecialidadMedica especialidadMedica) {
		this.EspecialidadMedica = especialidadMedica;
	}
	@ManyToOne 
	@JoinColumn(name="id_Especialista",nullable=false)
	private Especialista Especialista;
	
	@ManyToOne
	@JoinColumn(name="id_EspecialidadMedica",nullable =false)
	private EspecialidadMedica EspecialidadMedica;
	
	
	
	

}*/
