package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workshop.models.Especialista;

@Repository
public interface IEspecialistaDAO extends JpaRepository<Especialista,Integer> {

}
